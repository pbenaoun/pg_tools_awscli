FROM amazon/aws-cli:2.11.2
RUN amazon-linux-extras install epel
RUN yum -y update
RUN echo $'[pgdg14] \n\
name=PostgreSQL 14 for RHEL/CentOS 7 - x86_64 \n\
baseurl=http://download.postgresql.org/pub/repos/yum/14/redhat/rhel-7-x86_64 \n\
enabled=1 \n\
gpgcheck=0' > /etc/yum.repos.d/pgdg.repo
RUN yum -y install postgresql14 tar nano && yum clean all
